import axios from 'axios'

const axiosInstance = axios.create({
    baseURL: 'https://'
});

export default axiosInstance