import {createStore} from 'vuex'
import mutations from "@/store/mutations";
import actions from "@/store/actions";
import state from "@/store/state";


const Index = createStore({
    state,
    mutations,
    actions
})

export default Index