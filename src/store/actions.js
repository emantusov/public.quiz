//import axiosInstance from "@/axios"
import requestResult from "@/mok/mock";

export default {
    GET_QUESTIONS: async (context) => {
        /* axiosInstance.get('/ajax/get_quiz.php').then(
             (questions) => {
                 const data = questions.data
                 if (!data.questions) {
                     context.commit('SET_EMPTY_QUESTIONS')
                 } else {
                     context.commit('SET_QUESTIONS', data.questions)
                     context.commit('SET_USER_ID', data.userId)
                 }
             }
         ).catch(() => {
             context.commit('SET_EMPTY_QUESTIONS')
         })*/
        setTimeout(() => {
                context.commit('SET_QUESTIONS', requestResult.questions)
                context.commit('SET_USER_ID', requestResult.userId)
        },
            800)
    },
    SEND_RESULT: function (context, result) {
       /* axiosInstance.post('/ajax/put_quiz_result.php', {
            request: result
        })*/

        let countRight = 0
        result.answers.forEach((answer) => {
            if (answer.right) {
                countRight++;
            }
        })

        context.commit('SET_QUESTION_RESULT', countRight)
    }
}