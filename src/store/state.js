export default function state() {
    return {
        questions: [],
        questionResult: '',
        emptyQuestion: false,
        userId: null
    }
}