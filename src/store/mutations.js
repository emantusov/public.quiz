export default {
    SET_QUESTIONS: (state, questions) => {
        state.questions = questions
    },
    SET_QUESTION_RESULT: (state, countRight) => {
        state.questionResult = `${countRight} из ${state.questions.length}`
    },
    SET_EMPTY_QUESTIONS: (state) => {
        state.emptyQuestion = true
    },
    SET_USER_ID: (state, userid) => {
        state.userId = userid
    }
}