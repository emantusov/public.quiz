import {createApp} from 'vue'
import App from './App.vue'
import Index from "@/store/index.js";


createApp(App).use(Index).mount('#app')
